package com.notificationsservice.mail;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.notificationsservice.javaclass.Email;

@Service
public class MailComponent {
	private final JavaMailSender emailSender;

	@Autowired
	public MailComponent(JavaMailSender emailSender) {
		this.emailSender = emailSender;
	}

	private class SendEmail extends Thread {
		private Email email;

		public SendEmail(Email email) {
			this.email = email;
		}

		public void run() {
			sendSimpleMimeMessage(this.email.getTo(), this.email.getSubject(), this.email.getBody());
		}
	}

	private void sendSimpleMimeMessage(String to, String subject, String text) {
		try {
			MimeMessage message = emailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setFrom(new InternetAddress("notificationshome92@gmail.com", "Avisos TheFayreOne"));
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setText(text, true);

			emailSender.send(message);
		} catch (MessagingException exception) {
			exception.printStackTrace();
		} catch (UnsupportedEncodingException exception) {
			exception.printStackTrace();
		}
	}

	public void sendEmail(Email email) {
		SendEmail send = new SendEmail(email);
		send.start();
	}
}
