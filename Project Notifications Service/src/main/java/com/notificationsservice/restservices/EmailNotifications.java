package com.notificationsservice.restservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.notificationsservice.javaclass.Email;
import com.notificationsservice.mail.MailComponent;

@RestController
@RequestMapping("/api")
public class EmailNotifications {
	private final MailComponent mailComponent;
		
	@Autowired
	public EmailNotifications(MailComponent mailComponent) {
		this.mailComponent = mailComponent;
	}

	@RequestMapping(value = "/sendemail", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public Email sendEmail(@RequestBody Email email) {
		mailComponent.sendEmail(email); 
		return email;
	}
}
