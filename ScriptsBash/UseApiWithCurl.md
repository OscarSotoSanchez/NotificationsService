# Use the api with Curl in bash

`curl --insecure --user USER:PASS -H "Content-Type: application/json" -X POST -d '{"to": "EMAIL", "subject": "SUBJECT", "body": "BODY"}' https://IP:8443/api/sendemail`

- Replace USER and PASS from the user with credencial in the api
- Replace EMAIL for the email to send
- Replace SUBJECT for the subject email
- Replace BODY for the message email
- Replace IP for the server's api running